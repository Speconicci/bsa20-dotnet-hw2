﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private string logPath;
        public string LogPath => logPath;

        public LogService(string logPath)
        {
            this.logPath = logPath;
        }

        public void Write(string logInfo)
        {
            using StreamWriter streamWriter = new StreamWriter(logPath, true);
            streamWriter.WriteLine(logInfo);
            streamWriter.Close();
        }

        public string Read()
        {
            string readedLogInfo = null;
            using (StreamReader streamReader = new StreamReader(logPath))
            {
                if (streamReader == null) throw new InvalidOperationException();
                readedLogInfo = streamReader.ReadToEnd();
                streamReader.Close();
            }
            return readedLogInfo;
        }
    }
}