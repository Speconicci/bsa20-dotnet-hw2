﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        Parking parkingInstance;
        TimerService withdrawTimer;
        TimerService logTimer;
        LogService logService;
        List<TransactionInfo> currentTransactionsList;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            Parking.GetInstance();
            this.parkingInstance = Parking.instance;
            this.logService = (LogService)logService;
            this.withdrawTimer = (TimerService)withdrawTimer;
            withdrawTimer.Elapsed += RegularWithdraw;
            this.logTimer = (TimerService)logTimer;
            logTimer.Elapsed += LogCurrentTransactions;
            currentTransactionsList = new List<TransactionInfo>();
            withdrawTimer.Start();
            logTimer.Start();
        }

        public ParkingService()
        {
            Parking.GetInstance();
            this.parkingInstance = Parking.instance;
            this.logService = new LogService(Settings.logFilePath);
			this.logService = new LogService(Settings.logFilePath);
            this.withdrawTimer = new TimerService(Settings.withdrawIntervalInMillis);
            withdrawTimer.Elapsed += RegularWithdraw;
            withdrawTimer.SignHandler();
            this.logTimer = new TimerService(Settings.logIntervalInMillis);
            logTimer.Elapsed += LogCurrentTransactions;
            logTimer.SignHandler();
            currentTransactionsList = new List<TransactionInfo>();
            withdrawTimer.Start();
            logTimer.Start();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            foreach (Vehicle v in parkingInstance.vehiclesOnParking)
            {
                if (v.Id == vehicle.Id) throw new ArgumentException();
            }
            parkingInstance.vehiclesOnParking?.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            parkingInstance.vehiclesOnParking.Remove(FindVehicleById(vehicleId));
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            FindVehicleById(vehicleId).Balance += sum;
        }

        private Vehicle FindVehicleById(string id)
        {
            foreach (Vehicle v in parkingInstance.vehiclesOnParking)
            {
                if (v.Id == id)
                {
                    return v;
                }
            }
            throw new ArgumentException();
        }

        public decimal GetBalance()
        {
            return parkingInstance.Balance;
        }

        public int GetCapacity()
        {
            return parkingInstance.vehiclesOnParking.Capacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - parkingInstance.vehiclesOnParking.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return currentTransactionsList?.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> vehicles = new ReadOnlyCollection<Vehicle>(parkingInstance.vehiclesOnParking);
            return vehicles;
        }

        public string ReadFromLog()
        {
            StreamReader streamReader = new StreamReader(logService.LogPath);
            return streamReader.ReadToEnd();
        }

        private void RegularWithdraw(object sender, ElapsedEventArgs e)
        {
            foreach (Vehicle v in parkingInstance.vehiclesOnParking)
            {
                decimal withdrawnSum = CalculateWithdrawnSum(v);
                v.Balance -= withdrawnSum;
                currentTransactionsList.Add(ComposeTransactionInfo(v, withdrawnSum));
                Console.WriteLine("{1} was withdrawn from the vehicle with id {0}. Vehicle balance is {2}", v.Id, withdrawnSum, v.Balance); // debug
            }
            Console.WriteLine("\n");
            TransactionInfo ComposeTransactionInfo(Vehicle vehicle, decimal withdrawnSum)
            {
                TransactionInfo transactionInfo = new TransactionInfo();
                transactionInfo.TimeOfTransaction = DateTime.Now.ToShortTimeString() + ' ' + DateTime.Now.ToShortDateString();
                transactionInfo.VehicleId = vehicle.Id;
                transactionInfo.VehicleBalance = vehicle.Balance;
                transactionInfo.WithdrawnSum = withdrawnSum;
                return transactionInfo;
            }

            decimal CalculateWithdrawnSum(Vehicle vehicle)
            {
                decimal withdrawnSum;
                Settings.vehiclesParkingTariffs.TryGetValue(vehicle.VehicleType, out withdrawnSum);
                if (vehicle.Balance >= withdrawnSum)
                {
                    return withdrawnSum;
                }
                else if(vehicle.Balance == 0)
                {
                    return withdrawnSum * Settings.penaltyMultiplier;
                }
                else if (vehicle.Balance < withdrawnSum && vehicle.Balance > 0)
                {
                    return (withdrawnSum - vehicle.Balance) * Settings.penaltyMultiplier + (withdrawnSum - vehicle.Balance);
                }
                else
                {
                    return Math.Abs(vehicle.Balance) * Settings.penaltyMultiplier;
                }
            }
        }

        private void LogCurrentTransactions(object sender, ElapsedEventArgs e)
        {
            logService.Write(FormateTransactionInfo());

            string FormateTransactionInfo()
            {
                TransactionInfo[] currentTransactionInfo = GetLastParkingTransactions();
                string logFileInfo = "";
                foreach (TransactionInfo ti in currentTransactionInfo)
                {
                    logFileInfo += "\n" + ti.TimeOfTransaction + "\n";
                    logFileInfo += "Id: "+ ti.VehicleId + "\n";
                    logFileInfo += "Balance: " + ti.VehicleBalance + "\n";
                    logFileInfo += "Withdrawn sum: " + ti.WithdrawnSum + "\n";
                }
                return logFileInfo;
            }
        }

        public void Dispose()
        {
            withdrawTimer.Dispose();
            logTimer.Dispose();
        }
    }
}
