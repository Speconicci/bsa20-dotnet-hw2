﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer timer;
        public double Interval { get => Interval; set => Interval = value; }

        public event ElapsedEventHandler Elapsed;

        public TimerService(double interval)
        {
            timer = new Timer(interval);
        }

        public void SignHandler()
        {
            timer.Elapsed += Elapsed;
        }

        public void Dispose()
        {
            this.timer.Dispose();
        }

        public void Start()
        {
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }
    }
}