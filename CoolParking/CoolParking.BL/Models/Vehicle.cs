﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using CoolParking.BL;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private string id;
        private readonly string idPattern = @"[A-Z]{2}[-][0-9]{4}[-][A-Z]{2}";
        private VehicleType vehicleType;
        private decimal balance;

        public string Id
        {
            get => id;
            set
            {
                if (Regex.IsMatch(value, idPattern))
                {
                    id = value;
                } 
                else throw new ArgumentException();
                
            }
        }

        public decimal Balance
        {
            get => balance;
            set => balance = value;
        }

        public VehicleType VehicleType
        {
            get => vehicleType;
        }

        public Vehicle(VehicleType vehicleType, decimal balance)
        {
            if (balance > 0) this.Balance = balance;
            else throw new ArgumentException();
            this.vehicleType = vehicleType;
            this.Id = GenerateRandomId();
        }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            this.Id = id;
            this.vehicleType = vehicleType;
            if (balance > 0) this.Balance = balance;
            else throw new ArgumentException();
        }

        public static string GenerateRandomId()
        {
            string id = "";
            Random random = new Random();

            id += GenRandomPreOrPostfix();
            id += AddSeparator();
            id += GenRandomCode();
            id += AddSeparator();
            id += GenRandomPreOrPostfix();

            return id;

            string GenRandomPreOrPostfix()
            {
                string result = "";

                result += (char)random.Next('A', 'Z');
                result += (char)random.Next('A', 'Z');

                return result;
            }
            string GenRandomCode()
            {
                string result = "";
                for (int i = 0; i < 4; i++)
                {
                    result += random.Next(0, 9);
                }
                return result;
            }
            char AddSeparator()
            {
                return '-';
            }
        }
    }
}