﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum
        {
            get; set;
        }
        private string timeOfTransaction;
        private string vehicleId;
        private decimal vehicleBalance;
        private decimal withdrawnSum;

        public string TimeOfTransaction { get => timeOfTransaction; set => timeOfTransaction = value; }
        public string VehicleId { get => vehicleId; set => vehicleId = value; }
        public decimal VehicleBalance { get => vehicleBalance; set => vehicleBalance = value; }
        public decimal WithdrawnSum { get => withdrawnSum; set => withdrawnSum = value; }
    }
}
