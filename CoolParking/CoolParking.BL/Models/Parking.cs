﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    // TODO: implement class Parking.
    //       Implementation details are up to you, they just have to meet the requirements 
    //       of the home task and be consistent with other classes and tests.

    public class Parking
    {
        public static Parking instance;
        private decimal balance;
        public List<Vehicle> vehiclesOnParking;

        private Parking()
        {
            this.balance = Settings.starterParkingBalance;
            this.vehiclesOnParking = new List<Vehicle>(Settings.parkingPlaces);
        }

        public decimal Balance
        {
            get => balance;
        }

        public static Parking GetInstance()
        {
            if (instance == null)
            {
                instance = new Parking();
                return instance;
            }
            else
            {
                return instance;
            }
        }
    }
}